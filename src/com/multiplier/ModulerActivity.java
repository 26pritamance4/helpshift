package com.multiplier;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class ModulerActivity extends Activity implements OnClickListener {
	private EditText etFloor, etCeling, etMultiplier;
	private Button btnGenerate;
	private ListView listView;
	private MultiPlierListAdapter adptr = null;
	private boolean flag = false;
	private int floorValue = 0, celingValue = 0, tempCelingValue = 0, multiplierValue;
	private ArrayList<Integer> val = new ArrayList<Integer>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_moduler);
		initView();
		clickEvent();
	}

	private void initView() {
		etFloor = (EditText) findViewById(R.id.etFloor);
		etCeling = (EditText) findViewById(R.id.etCeling);
		etMultiplier = (EditText) findViewById(R.id.etMultiplier);
		btnGenerate = (Button) findViewById(R.id.btnGenerate);
		listView = (ListView) findViewById(R.id.listView);
	}

	private void clickEvent() {
		btnGenerate.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (etFloor.getText().toString().trim().length() == 0 || etCeling.getText().toString().trim().length() == 0
				|| etMultiplier.getText().toString().trim().length() == 0) {
			Toast.makeText(getApplicationContext(), "DO NOT LEAVE ANY FIELD BLANK", Toast.LENGTH_LONG).show();
		} else {
			floorValue = Integer.parseInt(etFloor.getText().toString().trim());
			celingValue = Integer.parseInt(etCeling.getText().toString().trim());
			multiplierValue = Integer.parseInt(etMultiplier.getText().toString().trim());
			for (tempCelingValue = celingValue; tempCelingValue > celingValue - multiplierValue; tempCelingValue--) {
				if (tempCelingValue % multiplierValue == 0) {
					print(tempCelingValue, floorValue);
					break;
				}
			}
			if (celingValue == 0 || multiplierValue == 0) {
				Toast.makeText(getApplicationContext(), "CELING VALUE & MULTIPLIER VALUE \n CANNOT BE 0", Toast.LENGTH_LONG)
						.show();
			} else if (celingValue < floorValue) {
				Toast.makeText(getApplicationContext(), "CELING VALUE \n CAN'T BE SMALLER THAN FLOOR VALUE", Toast.LENGTH_LONG)
						.show();
			} else {
				adptr = new MultiPlierListAdapter(ModulerActivity.this, val);
				listView.setAdapter(adptr);
			}
		}

	}

	private void print(int localtempCelingValue, int localfloorValue) {
		if (!val.isEmpty())
		val.clear();
		for (; localtempCelingValue >= localfloorValue; localtempCelingValue = localtempCelingValue - multiplierValue) {
			if (localtempCelingValue != 0)
				val.add(localtempCelingValue);
		}
	}
}
