package com.multiplier;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MultiPlierListAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<Integer> val=new ArrayList<Integer>();

	public MultiPlierListAdapter(Context context, ArrayList<Integer> val) {
		this.context = context;
		this.val = val;
	}

	@Override
	public int getCount() {
		return val.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflator = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
			convertView = inflator.inflate(R.layout.adapterview, null);
		TextView tvValue=(TextView)convertView.findViewById(R.id.tvValue);
		tvValue.setText(val.get(position).toString());
		return convertView;
	}

}
